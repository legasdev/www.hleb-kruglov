/**
 *
 * Управление поведением на главной
 *
 */

const
    uriHash = ['', '#cakes', '#productions', '#job', '#about', '#reviews', '#contacts'],
    titles =  ['', 'Заказные торты / ', 'Продукция / ', 'Работа у нас / ', 'О нас / ', 'Отзывы / ', 'Контакты / '],
    title = 'Пекарни Круглова / Наш городецкий хлеб';

const pageSlider = {

    init(initialIndex) {
        this.currentIndex = initialIndex;
        this.isChange = true;
        this.isChangeException = true;
        this.isWidthException = true;
        this.slides = [...document.querySelectorAll('.js-page-slider-slide')];
        this.lengthSlides = this.slides.length;
        this.exceptions = [...document.querySelectorAll('.js-page-slider-exceptions')];
        this.buttons = [...document.querySelectorAll('.js-page-slider-button')];
        this.background = document.querySelector('.background');
        this.video = document.querySelector('.video');
        this.mobileWidth = 1270;
        this.timeout;

        this.setIndex = this.setIndex.bind(this);
        this.onClickLink = this.onClickLink.bind(this);
        this.drawNewSlide = this.drawNewSlide.bind(this);
        this.addOnWheelEvent = this.addOnWheelEvent.bind(this);
        this.onWheel = this.onWheel.bind(this);
        this.onTransitionEndSlide = this.onTransitionEndSlide.bind(this);
        this.setChangeSlide = this.setChangeSlide.bind(this);
        this.setWidthException = this.setWidthException.bind(this);
        this.onScroll = this.onScroll.bind(this);

        this.buttons.forEach(button => button.addEventListener('click', this.onClickLink));
        this.slides.forEach(slide => slide.addEventListener('transitionend', this.onTransitionEndSlide));

        this.exceptions.forEach(domNode => {
            domNode.addEventListener('mouseenter', () => this.setChangeSlide(false));
            domNode.addEventListener('mouseleave', () => this.setChangeSlide(true));
        });

        window.addEventListener('scroll', this.onScroll);
        window.addEventListener('resize', this.setWidthException);

        if (window.innerWidth <= this.mobileWidth) {
            this.isWidthException = false;
        } else {
            this.setIndex(this.currentIndex);
            this.drawNewSlide();
        }

        this.addOnWheelEvent(document);

        return this;
    },

    /**
     * Устанавливает новый активный индекс
     *
     * @param {Number} newIndex - новый индекс
     */
    setIndex(newIndex) {
        if (this.isChange && this.isChangeException && this.isWidthException) {
            this.isChange = false;

            if (newIndex >= 0 && newIndex < this.lengthSlides) {
                this.currentIndex = newIndex;
            } else if (newIndex < 0) {
                this.currentIndex = this.lengthSlides - 1;
            } else {
                this.currentIndex = 0;
            }

            history.pushState(null, null, uriHash[this.currentIndex] || '/');
            document.title = `${titles[this.currentIndex]}${title}`;
            this.drawNewSlide();
        }
    },

    /**
     * Обработчик нажатия ссылке в меню
     *
     * @param {Event} event - событие клика
     */
    onClickLink(event) {
        const
            target = event.target,
            indexTarget = this.buttons.indexOf(target);

        this.setIndex(indexTarget);

        if (this.isWidthException) {
            event.preventDefault();
        }
    },

    /**
     * Обновляет классы у фона-полукруга и у ссылок
     * Скрывает видео, если слайд не первый
     *
     */
    drawNewSlide() {
        const
            activeClassButton = 'navigate__link--active',
            activeClassSlide = 'js-page-slider-slide--hide';

        (this.currentIndex + 1) % 2 === 0
            ? this.background.classList.add('background--right')
            : this.background.classList.remove('background--right');

        this.currentIndex !== 0
            ? this.video.classList.add('video--hide')
            : this.video.classList.remove('video--hide');

        this.buttons.forEach(button => button.classList.remove(activeClassButton));
        this.buttons[this.currentIndex]?.classList.add(activeClassButton);

        this.slides.forEach(slide => slide?.classList.add(activeClassSlide));
        this.slides[this.currentIndex]?.classList.remove(activeClassSlide);
    },

    /**
     * Добавление слушателя прокрутки
     *
     * @param elem - Элемент, к которому цепляем слушателя
     */
    addOnWheelEvent(elem) {
        if (elem.addEventListener) {
            if ('onwheel' in document) {
                // IE9+, FF17+, Ch31+
                elem.addEventListener("wheel", this.onWheel);
            } else if ('onmousewheel' in document) {
                // устаревший вариант события
                elem.addEventListener("mousewheel", this.onWheel);
            } else {
                // Firefox < 17
                elem.addEventListener("MozMousePixelScroll", this.onWheel);
            }
        } else { // IE8-
            elem.attachEvent("onmousewheel", this.onWheel);
        }
    },

    /**
     * Событие при прокрутке колесика мыши
     *
     * @param event
     */
    onWheel(event) {
        const
            eventWheel = event || window.event;

        clearTimeout(this.timeout);
        this.timeout = null;


        const
            delta = eventWheel.deltaY || eventWheel.detail || eventWheel.wheelDelta;

        if (this.isWidthException) {
            this.timeout = setTimeout(this.onTransitionEndSlide, 200);

            delta > 0
                ? this.setIndex(this.currentIndex + 1)
                : this.setIndex(this.currentIndex - 1);
        } else {

        }
    },

    /**
     * Закончилось переклчение слайда
     *
     */
    onTransitionEndSlide() {
        this.isChange = true;
    },

    /**
     * Разрешает или запрещает переключение слайдов
     *
     * @param newValue
     */
    setChangeSlide(newValue) {
        this.isChangeException = newValue;
    },

    /**
     * Запрещает переключение на маленьких экранах
     *
     * @param newValue
     */
    setWidthException(newValue) {
        if (window.innerWidth <= this.mobileWidth) {
            this.isWidthException = false;
        } else if (!this.isWidthException) {
            this.isWidthException = true;
        }
    },

    /**
     * Скролл на телефоне
     *
     */
    onScroll() {
        const
            body = document.body,
            docEl = document.documentElement,
            scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;

        this.currentIndex =
            this.slides
                .filter(slide => getCoords(slide).top < scrollTop)
                .length || 0;

        if (this.currentIndex >= this.lengthSlides) {
            this.currentIndex = this.lengthSlides - 1;
        }

        history.pushState(null, null, uriHash[this.currentIndex] || '/');
        document.title = `${titles[this.currentIndex]}${title}`;
        this.drawNewSlide();
    },

};

function getCoords(elem) {
    const
        box = elem.getBoundingClientRect(),
        body = document.body,
        docEl = document.documentElement,

        scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop,
        scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft,

        clientTop = docEl.clientTop || body.clientTop || 0,
        clientLeft = docEl.clientLeft || body.clientLeft || 0,

        top = box.top + scrollTop - clientTop,
        left = box.left + scrollLeft - clientLeft;

    return {
        top: top,
        left: left
    };
}

const
    slider = Object.create(pageSlider).init(uriHash.indexOf(location.hash) || 0);