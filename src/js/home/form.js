/**
 *
 * Обработка форм
 *
 */

import {sendForm, serialize, setStatusInButton, validate} from "@js/common/form";
import notify from './../common/notify';

const
    forms = [...document.querySelectorAll('.form')];

forms.forEach(item => {
    item.onsubmit = onSubmit;
});

async function onSubmit(event) {
    event.preventDefault();

    const
        form = event.target,
        button = form.querySelector('button'),
        successText = [...form.classList].includes('contacts__form')
            ? 'Сообщение отправлено. Скоро мы его рассмотрим. Спасибо.'
            : 'Отзыв успешно отправлен. Спасибо.';

    setStatusInButton(button, true);

    try {
        if (validate(form)) {

            dataLayer.push({
                'event': [...form.classList].includes('contacts__form') ? 'send_contacts_form' : 'send_review_form',
            });

            const
                data = serialize(form),
                result = await sendForm(form, 'post', data, false);

            if (result.ok) {
                notify.addNotify(successText);
                form.reset();
            } else {
                notify.addNotify('Произошла ошибка. Мы уже работаем над этим.');
            }

            form.reset();
        }
    } catch(error) {
        console.error(error);
    }

    setStatusInButton(button, false);
}



