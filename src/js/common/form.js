/**
 *
 * Работа с формой
 *
*/

import patterns from "@js/common/patterns";
import {sendAjax} from "@js/common";
import {validateTel} from "@js/common/validate";

/** PRIVATE **/

function checkIsExcludeInput(input, isValidate) {
    return isValidate
        ? (!input.name || input.disabled || input.type === 'submit' || input.type === 'checkbox' || input.name === 'g-recaptcha-response')
        : (!input.name || input.disabled || input.type === 'submit' || input.name === 'g-recaptcha-response');
}

function validateInput(value, type) {
    switch (type) {
        case 'email':
            return {
                ok: checkIsCorrectEmail(value)
            };

        case 'text': {
            const minCheck = checkIsCorrectMsg(value, 2, true);
            return {
                ok: minCheck && checkIsCorrectMsg(value, 1000, false),
                param: minCheck
            };
        }

        case 'tel': {
            return {
                ok: validateTel(value)
            };
        }

        case 'textarea': {
            const minCheck = checkIsCorrectMsg(value, 5, true);
            return {
                ok: minCheck && checkIsCorrectMsg(value, 1000, false),
                param: minCheck
            };
        }

        default:
            return {ok: true};
    }
}

function checkIsCorrectEmail(value='') {
    return (!!value.match(patterns.emailPattern) && value.length > 3);
}

function checkIsCorrectMsg(value='', numSymbols=5, isMin=true) {
    return isMin ? value.length > numSymbols : value.length < numSymbols;
}

function clearErrorInput(event) {
    const
        input = event.target;

    input.parentNode.classList.remove('input--error');
    input.removeEventListener('input', clearErrorInput);
}

function simulateAJAX(delay, result) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result);
        }, delay);
    });
}

/** PUBLIC **/

export function setStatusInButton(button, status) {
    status
        ? button.classList.add('button--load')
        : button.classList.remove('button--load');
}

export function serialize(form) {
    return Array.from(form.elements).reduce( (acc, field) => {

        if ( checkIsExcludeInput(field, false) ) return acc;

        return {
            ...acc,
            [field.name]: (field.type === 'checkbox') ? field.checked : field.value
        }

    }, {});
}

export function validate(form) {
    let
        isSuccess = true;

    [...form.elements].forEach( element => {

        if ( !checkIsExcludeInput(element, true) ) {
            const
                type = element.type,
                value = element.value,
                result = validateInput(value, type);

            if ( !result.ok ) {
                isSuccess = false;
                element.parentNode.classList.add('input--error');

                // Add a handler to check for clicks (error reset)
                element.addEventListener('input', clearErrorInput);
            }
        }

    });

    return isSuccess;
}

export async function sendForm(form, method, body, isSimulate=false) {

    const
        url = form.getAttribute('action');

    try {
        return isSimulate
            ? await simulateAJAX(1000, {status: true})
            : await sendAjax(url, method, body);
    } catch(error) {
        console.error(error);
        return {ok: false};
    }
}