/**
 *
 * Простой слайдер переключения фоток
 *
 */

const
    mainSliderClass = 'js-simple-slider',
    slideClass = 'js-simple-slider__slide',
    slideActiveClass = 'js-simple-slider__slide--active';

const simpleSlider = {

    init(domNode, delay=4000) {
        this.node = domNode;
        this.slides = [...this.node.querySelectorAll(`.${slideClass}`)] || [];
        this.lengthSlides = this.slides.length;
        this.currentIndex = 0;
        this.timer = null;
        this.delay = delay;

        this.setIndex = this.setIndex.bind(this);
        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this.draw = this.draw.bind(this);

        this.start();
        this.draw();

        return this;
    },

    /**
     * Устанавливает новый активный индекс
     *
     * @param {Number} newIndex - новый индекс
     */
    setIndex(newIndex) {
        if (newIndex >= 0 && newIndex < this.lengthSlides) {
            this.currentIndex = newIndex;
        } else if (newIndex < 0) {
            this.currentIndex = this.lengthSlides - 1;
        } else {
            this.currentIndex = 0;
        }

        this.draw();
    },

    /**
     * Запуск листания слайдера
     *
     */
    start() {
        this.timer = setInterval(() => this.setIndex(this.currentIndex + 1), this.delay);
    },

    /**
     * Останавливает листание в слайдере
     *
     */
    stop() {
        clearInterval(this.timer);
        this.timer = null;
    },

    /**
     * Обновляет состояние слайдов на странице
     *
     */
    draw() {
        this.slides.forEach(slide => slide.classList.remove(slideActiveClass));
        this.slides[this.currentIndex].classList.add(slideActiveClass);
    },
};

const
    sliders = [...document.querySelectorAll(`.${mainSliderClass}`)]
        .map(domNodeSlider => Object.create(simpleSlider).init(domNodeSlider));