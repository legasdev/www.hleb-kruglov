import React, {useCallback, useEffect, useState} from "react";

/**
 * Селектор выбора нескольких элементов
 *
 * @param {String} name - Имя селекта
 * @param {String | Number} id - Уникальный ID элемента
 * @param {Array} values - Массив со список выбора ({name: Отображаемое имя, value: Значение в коде})
 * @param {Array} selectedValues - Выбранные элемент ({name: Отображаемое имя, value: Значение в коде})
 * @param {Boolean} isMini - Внешне выглядит меньше и уже
 * @param {String} label - Подпись сверху
 * @param {Function} handleChange - Функция-коллбэк
 * @param {Object} style - Стили для всего селекта
 * @param {Boolean} isDisabled - Можно ли взаимодействовать
 */
const MultiSelect = ({name='', id=Math.floor(Math.random() * Date.now()), values=[],
                         selectedValues=[], isMini=false, label, handleChange, style, isDisabled=false }) => {

    const
        [isOpen, setIsOpen] = useState(false);

    const
        onOpenSelect = useCallback(() => {
            if (!isDisabled) {
                setIsOpen(!isOpen);
            }
        }, [isOpen, isDisabled]),

        onBlur = useCallback(() => {
            setIsOpen(false);
        }, []),

        onClickOption = useCallback((value, screenName, isSelected) => {
            if (!selectedValues.find(item => item.value === value) || !isSelected) {
                handleChange && handleChange(name, value, screenName, isSelected);
            }
        }, [selectedValues, handleChange]);

    return (
        <div
            className="select select--styles"
            onBlur={onBlur}
            style={style}
        >
            {
                label &&
                <label
                    className="input__label"
                    htmlFor={id}
                >{label}</label>
            }
            <div
                className={`select__header ${
                    isMini ? 'select__header--min' : ''} ${
                    isOpen ? 'select__header--open' : ''}`
                }
            >
                <div
                    className={'select__multi_back'}
                    onClick={onOpenSelect}
                />
                <div className={'select__multi'}>
                    {
                        selectedValues &&
                        selectedValues.map(value => (
                            <div className={'select__multi_item'} key={value.value}>
                                <span className={'select__multi_text'}>{value.name}</span>
                                <div
                                    className={'select__multi_cross'}
                                    onClick={() => onClickOption(value.value, value.name, false)}
                                />
                            </div>
                        ))
                    }
                </div>
                <div
                    className={`select__arrow ${isOpen ? 'select__arrow--open' : ''}`}
                    onClick={onOpenSelect}
                />
            </div>
            <div
                className={`select__wrapper ${isOpen ? 'select__wrapper--open' : ''}`}
            >
                <div>
                    {
                        values &&
                        values.map(item => (
                            <div
                                key={item.value}
                                className={`select__option ${
                                    selectedValues
                                        .find(value => value.value === item.value)
                                        ?.selected ? 'select__option--selected' : ''
                                }`}
                                onClick={() => onClickOption(item.value, item.name, true)}
                            >
                                {item.name}
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    );

};

export default MultiSelect;