import React, {useState, useCallback} from "react";

const Input = ({id=Math.floor(Math.random() * Date.now()), name, type='text', label,
                   errorText="Неправильно заполнено поле", handleChange, outValue,
               isNotValid=false, ...props}) => {

    const
        [value, setValue] = useState('');

    const
        onChange = useCallback((event) => {
            outValue && setValue(event.target.value);
            handleChange && handleChange(name, event.target.value);
        }, [handleChange, outValue]);

    return (
        <div className={`input input--styles ${isNotValid ? 'input--error' : ''}`}>
            {
                label &&
                <label className="input__label" htmlFor={id}>{label}</label>
            }
            <input
                className="input__text"
                type={type}
                id={id}
                name={name}
                value={outValue ?? value}
                onChange={onChange}
                {...props}
            />
            <span className="input__error">{errorText}</span>
        </div>
    );
};

export default Input;