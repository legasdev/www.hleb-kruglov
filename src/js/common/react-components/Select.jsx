import React, {useState, useEffect, useCallback} from "react";

import '@less/common/form/select.less';

/**
 * Селектор одиночного выбора
 * (можно выбрать только один элемент)
 *
 * @param {String} name - Имя селекта
 * @param {String | Number} id - Уникальный ID элемента
 * @param {Array} values - Массив со список выбора ({name: Отображаемое имя, value: Значение в коде})
 * @param {String} selectedValue - Выбранный элемент (value)
 * @param {Boolean} isMini - Внешне выглядит меньше и уже
 * @param {String} label - Подпись сверху
 * @param {Function} handleChange - Функция-коллбэк
 * @param {Object} style - Стили для всего селекта
 * @param {Object} styleActive - Стили для выбранного элемента (вверху селекта)
 * @param {Boolean} isDisabled - Можно ли взаимодействовать
 */
const Select = ({name='', id=Math.floor(Math.random() * Date.now()), values=[],
                    selectedValue, isMini=false, label, handleChange, style, styleActive, isDisabled=false }) => {

    const
        [isOpen, setIsOpen] = useState(false),
        [selectedName, setSelectedName] = useState(values &&
            (!!selectedValue
                ? values.filter(item => item.value === selectedValue)[0].name
                : values.length > 0 ? values[0].name : '')
        );

    const
        onOpenSelect = useCallback(() => {
            if (!isDisabled) {
                setIsOpen(!isOpen);
            }
        }, [isOpen, isDisabled]),

        onClickOption = useCallback((value, nameValue) => {
            setSelectedName(nameValue);
            handleChange && handleChange(name, value, nameValue);
        }, [name, handleChange]),

        onBlur = useCallback(() => {
            setIsOpen(false);
        }, []);

    useEffect(() => {
        if (selectedName && selectedName.length === 0) {
            setSelectedName(values[0].name)
        } else if (!selectedName && values && values.length > 0) {
            setSelectedName(values[0].name)
        }
    }, [values, selectedName]);

    return (
        <div
            className="select select--styles"
            onClick={onOpenSelect}
            onBlur={onBlur}
            style={style}
        >
            {
                label &&
                <label
                    className="input__label"
                    htmlFor={id}
                >{label}</label>
            }
            <div
                className={`select__header ${
                    isMini ? 'select__header--min' : ''} ${
                    isOpen ? 'select__header--open' : ''}`
                }
            >
                <span
                    className='select__selected-value'
                    style={styleActive}
                >{selectedName}</span>
                <div className={`select__arrow ${isOpen ? 'select__arrow--open' : ''}`} />
            </div>
            <div className={`select__wrapper ${isOpen ? 'select__wrapper--open' : ''}`}>
                <div>
                    {
                        values &&
                        values.map(item => (
                            <div
                                key={item.value}
                                className={`select__option ${item.name === selectedName ? 'select__option--selected' : ''}`}
                                onClick={() => onClickOption(item.value, item.name)}
                            >
                                {item.name}
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    );
};

export default Select;