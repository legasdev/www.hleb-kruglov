import React, {useState, useRef, useCallback} from "react";

const InputFile = ({id=Math.floor(Math.random() * Date.now()), name, label, handleChange, isNotValid=false}) => {

    const
        fileRef = useRef(null);

    const
        [filesList, setFilesList] = useState(null),
        [previewList, setPreviewList] = useState(null);

    const
        readImage = useCallback((file) => {
            const img = new Image();
            img.addEventListener('load', () => {
                window.URL.revokeObjectURL(img.src); // Free some memory
            });
            img.src = window.URL.createObjectURL(file);
            return img.src;
        }, []),

        onChange = useCallback(() => {
            // File Select Event
            const
                files = fileRef.current.files;

            setFilesList(files);
            handleChange && handleChange(name, files);

            setPreviewList([...files].map((file) => ({
                src: readImage(file),
                name: file.name
            })));

        }, [fileRef, readImage]),

        onClickImg = useCallback((src, nameImg) => {
            const
                newFilesList = [...filesList].filter(img => img.name !== nameImg);

            setFilesList(newFilesList);
            handleChange && handleChange(name, newFilesList);

            setPreviewList(previewList.filter(img => img.src !== src));
        }, [previewList, filesList, handleChange]);

    return (
        <div className={`input input--styles ${isNotValid ? 'input--error' : ''}`}>
            {
                label &&
                <label className="input__label" htmlFor={id}>{label}</label>
            }
            <input
                type={'file'}
                className={'input__file_item--display-none'}
                id={id}
                name={name}
                multiple
                ref={fileRef}
                accept={"image/*"}
                onChange={onChange}
            />
            <div className={'input__file'}>
                {
                    previewList &&
                    previewList.map(img => (
                        <div
                            key={img.src}
                            className={'input__file_item input__file_item--img'}
                            onClick={() => {onClickImg(img.src, img.name)}}
                        >
                            <img
                                className={'input__file_item'}
                                src={img.src}
                                alt={''}
                            />
                            <div className={'input__file_item--shadow'} />
                        </div>
                    ))
                }
                <label className="input__file_item input__file_item--button" htmlFor={id}>
                    <span className="input__file_text">Нажмите, чтобы добавить</span>
                </label>
            </div>
        </div>
    );

};

export default InputFile;