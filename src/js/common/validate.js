export function validateEmail(value) {
    return /[a-zA-Z0-9\.\_\-]+@[a-zA-Z0-9]+\.[a-zA-Z]+/gi.test(value);
}

export function validateTel(value) {
    return value.length > 4 && value.length < 20;
}

export default function valid(key, value) {

    switch (key) {
        case 'email':
            return !validateEmail(value);

        case 'tel':
            return !validateTel(value);

        case 'item':
            return value.length < 5;

        case 'amount':
            return value.length <= 0 || (parseFloat(value) < 50 && parseFloat(value) >= 50000);

        default:
            return value.length <= 3;
    }
}
