/**
 *
 * Управление меню для телефонов
 *
 */

const
    button = document.querySelector('.navigate__button'),
    links = [...document.querySelectorAll('.navigate__link')],
    menuWrapper = document.querySelector('.navigate__menu');

links.forEach(link => link.addEventListener('click', () => {
    button.classList.remove('navigate__button--open');
    menuWrapper.classList.remove('navigate__menu--open');
    document.body.classList.remove('body--overflow');
}));

button.addEventListener('click', () => {
    button.classList.toggle('navigate__button--open');
    menuWrapper.classList.toggle('navigate__menu--open');
    document.body.classList.toggle('body--overflow');
});