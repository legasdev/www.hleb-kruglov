/**
 *
 * Уведомления
 *
 */

const notifyMain = {

    init() {
        this.node = document.querySelector('.notify');
        this.list = [];

        this.addNotify = this.addNotify.bind(this);
        this.updatePosition = this.updatePosition.bind(this);
        this.removeNotifyFromList = this.removeNotifyFromList.bind(this);

        return this;
    },

    addNotify(text='', timeOut=3000) {
        const
            newNotify = Object.create(notifyBlock).init(text, this.list.length, this, timeOut);

        this.list = [...this.list, newNotify];

        this.node.appendChild(newNotify.getNode());

        setTimeout(() => this.updatePosition(), 40);
    },

    updatePosition() {
        this.list.forEach((notify, i) => {
            notify.setNewPosition(0, 20 * (i + 1) + notify.getHeight() * i);
        });
    },

    removeNotifyFromList(id) {
        this.list = this.list.filter(notify => notify.id !== id);
        this.updatePosition();
    },
};

const notifyBlock = {

    init(text, id, parent, timeOut=3000) {
        this.node = document.createElement('div');
        this.id = id;
        this.parent = parent;

        this._getTemplate = this._getTemplate.bind(this);
        this._destroyThis = this._destroyThis.bind(this);
        this.getNode = this.getNode.bind(this);
        this.close = this.close.bind(this);
        this.getHeight = this.getHeight.bind(this);

        this.node.classList.add('notify__block');
        this.node.innerHTML = this._getTemplate(text);

        this.closeButton = this.node.querySelector('.notify__close');

        this.closeButton.addEventListener('click', this.close);

        this.timer = setTimeout(this.close, timeOut);

        return this;
    },

    _getTemplate(text='') {
        return `<span>${text}</span><div class="notify__close"><i></i><i></i></div>`;
    },

    getNode() {
        return this.node;
    },

    getHeight() {
        return this.node.offsetHeight;
    },

    setNewPosition(x=0, y=0) {
        this.node.style.transform = `translate3d(${x}px, ${y}px, 0)`;
    },

    close() {
        this.node.style.transform = `translate3d(0, -200px, 0)`;

        this.node.addEventListener('transitionend', this._destroyThis);
    },

    _destroyThis() {
        clearTimeout(this.timer);
        this.timer = null;
        this.closeButton.removeEventListener('click', this.close);
        this.node.removeEventListener('transitionend', this._destroyThis);
        this.node.remove();

        this.parent.removeNotifyFromList(this.id);
    },
};

export default Object.create(notifyMain).init();
