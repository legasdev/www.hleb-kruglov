import React, {useCallback, useState} from "react";
import Form from "@js/custom/form/components/Form";
import notify from "@js/common/notify";

const App = props => {

    const
        [isFetch, setIsFetch] = useState(false);

    const
        onSendPay = useCallback(async (data) => {
            setIsFetch(true);

            dataLayer.push({
                'value': data.baseCake.name,
                'event': 'selected_base_cake',
            });

            data.decor.forEach(({name}) => {
                dataLayer.push({
                    'value': name,
                    'event': 'selected_decor',
                });
            });

            data.toppings.forEach(({name}) => {
                dataLayer.push({
                    'value': name,
                    'event': 'selected_toppings',
                });
            });

            const
                formData = new FormData(),
                dataToFetch = {
                    ...data,
                    baseCake: data.baseCake.name,
                    toppings: data.toppings.map(item => item.name),
                    decor: data.decor.map(item => item.name),
                };

            delete dataToFetch.files;

            formData.append('properties', new Blob([JSON.stringify(dataToFetch)], {
                type: "application/json"
            }));

            for (let file of data.files) {
                formData.append('file', file);
            }

            dataLayer.push({
                'event': 'click_cake_order',
            });

            try {
                const
                    result = await fetch('/api/custom/new', {
                        method: 'POST',
                        cache: 'no-cache',
                        body: formData,
                    });

                if (result.ok) {
                    dataLayer.push({
                        'status': 'success',
                        'event': 'cake_order',
                    });
                    notify.addNotify('Спасибо, ваша заявка получена! Скоро наш администратор (менеджер) свяжется для подтверждения заказа в течении 30 минут.', 60000);
                } else {
                    notify.addNotify('Серверная ошибка.');
                    throw new Error('Серверная ошибка.');
                }
            } catch (error) {
                dataLayer.push({
                    'status': 'reject',
                    'event': 'cake_order',
                });

                console.group('=== Ошибка заказа торта ===');
                console.error(error);
                console.groupEnd();
            }

            setIsFetch(false);
        }, []);

    return (
        <Form handleSubmit={onSendPay} isFetch={isFetch} />
    );

};

export default App;
