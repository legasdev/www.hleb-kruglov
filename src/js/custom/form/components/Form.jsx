import React, {useState, useCallback, useReducer} from "react";
import reducer from "./formReducer";
import notify from '@js/common/notify';
import valid from "@js/common/validate";
import {getDate} from "@js/common";

import baseCakeSelect from "@js/custom/form/components/baseCakeSelect";
import toppingsSelect from "@js/custom/form/components/toppingsSelect";
import decorSelect from "@js/custom/form/components/decorSelect";

import Input from "@js/common/react-components/Input";
import Textarea from "@js/common/react-components/Textarea";
import Select from "@js/common/react-components/Select";
import MultiSelect from "@js/common/react-components/MultiSelect";
import InputFile from "@js/common/react-components/InputFile";

const initialState = {
    name: '',
    description: '',
    email: '',
    phone: '',
    baseCake: baseCakeSelect[0],
    toppings: [{
        ...toppingsSelect[0],
        selected: true,
    }],
    decor: [{
        ...decorSelect[0],
        selected: true,
    }],
    date: getDate(4),
    files: [],
};

const isNotValidInitial = {
    name: false,
    email: false,
    phone: false,
};

const Form = ({handleSubmit, isFetch=false}) => {

    const
        [state, dispatch] = useReducer(reducer, initialState),
        [isNotValidObject, setIsNotValidObject] = useState(isNotValidInitial);

    const
        validate = useCallback((type=null) => {

            return Object.keys(isNotValidObject).reduce((acc, key) => ({
                ...acc,
                [key]: type
                    ? type === key
                        ? valid(key, state[key])
                        : isNotValidObject[key]
                    : valid(key, state[key])
            }), {});

        }, [state, isNotValidObject]),

        onSubmit = useCallback((event) => {
            event.preventDefault();

            const
                {...data} = state;

            if (!isFetch) {
                setIsNotValidObject(validate());

                if (Object.values(validate()).includes(true)) {
                    notify.addNotify('Неверно заполнены поля.');
                    return;
                }

                handleSubmit(data);
            }
        }, [state, validate, isNotValidObject, isFetch]),

        onChangeInput = useCallback((name, newValue, nameValue='', isSelected) => {
            if (!isFetch) {
                dispatch({type: name, value: newValue, nameValue, isSelected});
                setIsNotValidObject(validate(name, newValue));
            }
        }, [validate, isFetch]);

    return (
        <form className="buy__form_wrapper" onSubmit={onSubmit}>
            <Input
                id={'buy-form_name'}
                label={'ФИО'}
                name={'name'}
                placeholder={'Например, Иванов Иван Иванович'}
                handleChange={onChangeInput}
                outValue={state.name}
                isNotValid={isNotValidObject.name}
            />
            <Input
                id={'buy-form_email'}
                label={'Email'}
                name={'email'}
                type={'email'}
                placeholder={'Например, mail@mail.ru'}
                handleChange={onChangeInput}
                outValue={state.email}
                isNotValid={isNotValidObject.email}
                errorText={'Неверная почта'}
            />
            <Input
                id={'buy-form_tel'}
                label={'Номер телефона'}
                name={'phone'}
                type={'tel'}
                placeholder={'Например, +78316123200'}
                handleChange={onChangeInput}
                outValue={state.phone}
                isNotValid={isNotValidObject.phone}
                errorText={'Неверный номер телефона'}
            />
            <Textarea
                id={'buy-form_description'}
                label={'Описание торта'}
                name={'description'}
                placeholder={'Например: Украсить половину торта ягодами, как водопад'}
                handleChange={onChangeInput}
                outValue={state.description}
            />
            <Select
                id={'buy-form_base-cake'}
                label={'Основа торта'}
                name={'baseCake'}
                values={baseCakeSelect}
                selectedValue={state.baseCake.value}
                handleChange={onChangeInput}
                isDisabled={isFetch}
                style={{
                    zIndex: '4'
                }}
            />
            <Select
                id={'buy-form_toppings'}
                label={'Прослойка'}
                name={'toppings'}
                values={toppingsSelect}
                selectedValues={state.toppings.value}
                handleChange={onChangeInput}
                isDisabled={isFetch}
                style={{
                    zIndex: '3'
                }}
            />
            <Select
                id={'buy-form_decor'}
                label={'Готовые начинки'}
                name={'decor'}
                values={decorSelect}
                selectedValues={state.decor.value}
                handleChange={onChangeInput}
                isDisabled={isFetch}
            />
            <Input
                id={'buy-form_date'}
                label={'К какому числу сделать торт'}
                name={'date'}
                type={'date'}
                placeholder={`Например, ${getDate(4)}`}
                min={getDate(4)}
                handleChange={onChangeInput}
                outValue={state.date}
            />
            <InputFile
                id={'buy-form_files'}
                label={'Добавьте фото похожих тортов'}
                name={'files'}
                handleChange={onChangeInput}
            />
            <button
                className={`button button--styles button--hover-white ${isFetch ? 'button--load' : ''}`}
                style={{marginTop: '40px'}}
                type='submit'
                disabled={isFetch}
            >
                <span>Заказать</span>
                <div className="spinner spinner--styles">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="23px" height="23px"
                         viewBox="0 0 128 128" xmlSpace="preserve">
                        <g>
                            <path
                                d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z"
                                fill="#c99837" fillOpacity="1"/>
                            <animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64"
                                              dur="1200ms" repeatCount="indefinite"/>
                        </g>
                    </svg>
                </div>
            </button>
            <div className={'buy__form_description'}>
                <p className="buy__form_description_text">Так же вы можете сделать заказ по телефону <a className={'buy__form_link'} href={"tel:89040608038"}>+7 (904) 060-80-38</a>.</p>
                <p className="buy__form_description_text">Все заказы принимаются: пн-пт с 09:00 до 16:30, кроме выходных.</p>
            </div>
        </form>
    );

};

export default Form;
