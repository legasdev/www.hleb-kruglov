export default (state, action) => {

    switch (action.type) {

        case 'name':
            return {
                ...state,
                name: action.value
            };

        case 'email':
            return {
                ...state,
                email: action.value
            };

        case 'phone':
            return {
                ...state,
                phone: action.value
            };

        case 'description':
            return {
                ...state,
                description: action.value
            };

        case 'date':
            return {
                ...state,
                date: action.value
            };

        case 'baseCake':
            return {
                ...state,
                baseCake: {
                    name: action.nameValue,
                    value: action.value
                }
            };

        case 'toppings':
            return {
                ...state,
                toppings: action.isSelected
                    ? [
                        ...state.toppings,
                        {
                            name: action.nameValue,
                            value: action.value,
                            selected: action.isSelected,
                        }
                    ]
                    : state.toppings.length <= 1
                        ? state.toppings
                        : state.toppings.filter(item => item.value !== action.value)
            };

        case 'decor':
            return {
                ...state,
                decor: action.isSelected
                    ? [
                        ...state.decor,
                        {
                            name: action.nameValue,
                            value: action.value,
                            selected: action.isSelected,
                        }
                    ]
                    : state.decor.length <= 1
                        ? state.decor
                        : state.decor.filter(item => item.value !== action.value)
            };

        case 'files':
            return {
                ...state,
                files: action.value
            };

        default:
            return {...state};
    }

};