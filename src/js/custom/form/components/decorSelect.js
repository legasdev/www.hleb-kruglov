export default [
    {
        name: 'При звонке',
        value: 'default',
        selected: false,
    },
    {
        name: 'Йогуртная',
        value: 'yoghurt',
        selected: false,
    },
    {
        name: 'Крем-Брюле',
        value: 'creme_brulee',
        selected: false,
    },
    {
        name: 'Медовый',
        value: 'honey',
        selected: false,
    },
    {
        name: 'Сливочно-творожная',
        value: 'creamy_curd',
        selected: false,
    },
    {
        name: 'Сметанная',
        value: 'sour_cream',
        selected: false,
    },
    {
        name: 'Сникерс',
        value: 'snickers',
        selected: false,
    },
    {
        name: 'Шоколадная',
        value: 'chocolate',
        selected: false,
    },
];
