export default [
    {
        name: 'При звонке',
        value: 'default',
        selected: false,
    },
    {
        name: 'Крем масляный',
        value: 'oil_cream',
        selected: false,
    },
    {
        name: 'Крем масляный + Вареная сгущенка',
        value: 'oil_cream_and_boiled_condensed_milk',
        selected: false,
    },
    {
        name: 'Начинка "Шоколад"',
        value: 'chocolate',
        selected: false,
    },
    {
        name: 'Взбитые сливки',
        value: 'whipped_cream',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Вареная сгущенка',
        value: 'whipped_cream_and_boiled_condensed_milk',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Конфитюр вишневый',
        value: 'whipped_cream_and_cherry_jam',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Конфитюр клубника',
        value: 'whipped_cream_and_strawberry_jam',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Конфитюр малиновый',
        value: 'whipped_cream_and_raspberry_jam',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Конфитюр черная смородина',
        value: 'whipped_cream_and_black_currant_jam',
        selected: false,
    },
    {
        name: 'Взбитые сливки + Сметана',
        value: 'whipped_cream_and_sour_cream',
        selected: false,
    },
];
