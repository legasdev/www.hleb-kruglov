export default [
    {
        name: 'При звонке',
        value: 'default'
    },
    {
        name: 'Белый бисквит',
        value: 'white_biscuit'
    },
    {
        name: 'Шоколадный бисквит',
        value: 'chocolate_biscuit'
    },
    {
        name: 'Белый + шоколадный бисквит',
        value: 'white_and_chocolate_biscuit'
    },
    {
        name: 'Медовая основа',
        value: 'honey_base'
    },
];
