import React from 'react';
import ReactDOM from 'react-dom';

import App from './form/App';

const
    targetDomNode = document.querySelector('.js-custom-form');

ReactDOM.render(
    <App />,
    targetDomNode
);