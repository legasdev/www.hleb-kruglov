export async function sendAjax(url, method, body) {
    return await fetch(url, {
        method,
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body),
    });
}

export function getDate(plusDays=0) {
    const
        date = new Date();

    date.setDate(date.getDate() + plusDays);

    const
        year = date.getFullYear(),
        month = (date.getMonth() + 1) < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1,
        day = date.getDate() < 10 ?  `0${date.getDate()}` : date.getDate();

    return `${year}-${month}-${day}`;
}