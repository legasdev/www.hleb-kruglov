import React, {useState, useCallback} from "react";
import notify from './../../common/notify';

import Form from "@js/buy/form/components/Form";

const App = props => {

    const
        [isFetch, setIsFetch] = useState(false),
        [link, setLink] = useState(null);

    const
        onSendPay = useCallback(async (data) => {
            setIsFetch(true);

            const
                sendData = {
                    ...data,
                    amount: parseFloat(data.amount) * 100
                },
                formData = new FormData();

            formData.append('properties', new Blob([JSON.stringify(sendData)], {
                type: "application/json"
            }));

            dataLayer.push({
                'event': 'click_on_payment',
            });

            try {
                const
                    result = await fetch('/api/buy/new', {
                        method: 'POST',
                        cache: 'no-cache',
                        body: formData,
                    });

                if (result.ok) {
                    const
                        data = await result.text();

                    dataLayer.push({
                        'status': 'success',
                        'event': 'get_link_payment',
                    });

                    setLink(data);
                    notify.addNotify('Ваша оплата обрабатывается...');
                    window.open(data, '_blank');
                } else {
                    notify.addNotify('Серверная ошибка оплаты.');
                    throw new Error('Серверная ошибка.');
                }
            } catch (error) {

                dataLayer.push({
                    'status': 'reject',
                    'event': 'get_link_payment',
                });

                console.group('=== Ошибка оплаты ===');
                console.error(error);
                console.groupEnd();
            }

            setIsFetch(false);
        }, []);

    return (
        <>
            <Form handleSubmit={onSendPay} isFetch={isFetch} />
            {
                link &&
                <div style={{
                    marginTop: '40px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    maxWidth: '650px'
                }}>
                    <span style={{textAlign: 'center'}}>Сейчас Вас переадресует на страницу оплаты.<br />Если этого не произошло, нажмите кнопку ниже</span>
                    <a
                        className={`button button--styles button--hover-white`}
                        style={{marginTop: '16px'}}
                        href={link}
                        target={'_blank'}
                        rel={"nofollow noreferrer"}
                        title={'Перейти к оплате'}
                    >
                        <span>Перейти к оплате</span>
                    </a>
                </div>

            }
        </>
    );

};

export default App;
