export default (state, action) => {

    switch (action.type) {

        case 'name':
            return {
                ...state,
                name: action.value
            };

        case 'email':
            return {
                ...state,
                email: action.value
            };

        case 'phone':
            return {
                ...state,
                phone: action.value
            };

        case 'amount':
            return {
                ...state,
                amount: action.value
            };

        case 'item':
            return {
                ...state,
                item: action.value,
            };

        case 'date':
            return {
                ...state,
                date: action.value,
            };

        default:
            return {...state};
    }

};