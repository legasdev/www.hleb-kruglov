import React, {useState, useCallback, useReducer} from "react";
import reducer from "./formReducer";
import notify from '@js/common/notify';
import valid from "@js/common/validate";
import {getDate} from "@js/common";

import Input from "@js/common/react-components/Input";
import Textarea from "@js/common/react-components/Textarea";

const initialState = {
    name: '',
    email: '',
    phone: '',
    item: '',
    date: getDate(),
    quantity: 1,
    amount: '',
};

const isNotValidInitial = {
    name: false,
    email: false,
    phone: false,
    item: false,
    amount: false,
};

const Form = ({handleSubmit, isFetch=false}) => {

    const
        [state, dispatch] = useReducer(reducer, initialState),
        [isNotValidObject, setIsNotValidObject] = useState(isNotValidInitial);

    const
        validate = useCallback((type=null) => {

            return Object.keys(isNotValidObject).reduce((acc, key) => ({
                    ...acc,
                    [key]: type
                        ? type === key
                            ? valid(key, state[key])
                            : isNotValidObject[key]
                        : valid(key, state[key])
                }), {});

        }, [state, isNotValidObject]),

        onSubmit = useCallback((event) => {
            event.preventDefault();

            const
                {...data} = state;

            if (!isFetch) {
                setIsNotValidObject(validate());

                if (Object.values(validate()).includes(true)) {
                    notify.addNotify('Неверно заполнены поля.');
                    return;
                }

                handleSubmit(data);
            }
        }, [state, validate, isNotValidObject, isFetch]),

        onChangeInput = useCallback((name, newValue, nameValue='') => {
            if (!isFetch) {
                dispatch({type: name, value: newValue, nameValue});
                setIsNotValidObject(validate(name));
            }
        }, [validate, isFetch]);

    return (
        <form className="buy-form__wrapper" onSubmit={onSubmit}>
            <Input
                id={'buy-form_name'}
                label={'ФИО'}
                name={'name'}
                placeholder={'Например, Иванов Иван Иванович'}
                handleChange={onChangeInput}
                outValue={state.name}
                isNotValid={isNotValidObject.name}
            />
            <Input
                id={'buy-form_email'}
                label={'Email'}
                name={'email'}
                type={'email'}
                placeholder={'Например, mail@mail.ru'}
                handleChange={onChangeInput}
                outValue={state.email}
                isNotValid={isNotValidObject.email}
                errorText={'Неверная почта'}
            />
            <Input
                id={'buy-form_tel'}
                label={'Номер телефона'}
                name={'phone'}
                type={'tel'}
                placeholder={'Например, +78316123200'}
                handleChange={onChangeInput}
                outValue={state.phone}
                isNotValid={isNotValidObject.phone}
                errorText={'Неверный номер телефона'}
            />
            <Textarea
                id={'buy-form_description'}
                label={'Описание заказа'}
                name={'item'}
                placeholder={'Например: Пироги с мясом 20шт. или Заказной торт'}
                handleChange={onChangeInput}
                outValue={state.item}
                isNotValid={isNotValidObject.item}
                errorText={'Минимальное количество символов: 5'}
            />
            <Input
                id={'buy-form_date'}
                label={'На какое число сделан заказ'}
                name={'date'}
                type={'date'}
                placeholder={`Например, ${getDate()}`}
                min={getDate()}
                handleChange={onChangeInput}
                outValue={state.date}
            />
            <Input
                id={'buy-form_amount'}
                label={'Сумма для оплаты (в рублях)'}
                name={'amount'}
                type={'number'}
                placeholder={'Например, 500'}
                handleChange={onChangeInput}
                outValue={state.amount}
                isNotValid={isNotValidObject.amount}
                min={"50"}
                max={"49999"}
                errorText={'Сумма должна быть от 50 до 50000 рублей'}
            />
            <button
                className={`button button--styles button--hover-white ${isFetch ? 'button--load' : ''}`}
                style={{marginTop: '40px'}}
                type='submit'
                disabled={isFetch}
            >
                <span>Оплатить</span>
                <div className="spinner spinner--styles">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="23px" height="23px"
                         viewBox="0 0 128 128" xmlSpace="preserve">
                            <g>
                                <path
                                    d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z"
                                    fill="#c99837" fillOpacity="1"/>
                                <animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64"
                                                  dur="1200ms" repeatCount="indefinite"/>
                            </g>
                        </svg>
                </div>
            </button>
        </form>
    );

};

export default Form;
