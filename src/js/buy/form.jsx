import React from 'react';
import ReactDOM from 'react-dom';

import App from './form/App';

const
    targetDomNode = document.querySelector('.js-buy-form');

ReactDOM.render(
    <App />,
    targetDomNode
);