/**
 *
 * Форма заявки на вакансию
 *
 */

import {sendForm, serialize, setStatusInButton, validate} from "@js/common/form";
import notify from './../common/notify';

const
    forms = [...document.querySelectorAll('form')];

forms.forEach(item => {
    item.onsubmit = onSubmit;
});

async function onSubmit(event) {
    event.preventDefault();
    const
        form = event.target,
        button = form.querySelector('button'),
        successText = 'Заявка отправлена. Спасибо.',
        errorText = 'Произошла ошибка. Мы уже работаем над этим.';

    setStatusInButton(button, true);

    try {
        if (validate(form)) {

            dataLayer.push({
                'event': 'send_job_form',
            });

            const
                data = serialize(form),
                result = await sendForm(form, 'post', data, false);

            if (result.ok) {
                notify.addNotify(successText);
                form.reset();
            } else {
                notify.addNotify(errorText);
            }
        }
    } catch(error) {
        console.error(error);
    }

    setStatusInButton(button, false);
}



