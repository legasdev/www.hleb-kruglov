const
    path = require("path"),
    BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
    { CleanWebpackPlugin } = require('clean-webpack-plugin'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    TerserWebpackPlugin = require('terser-webpack-plugin'),
    autoprefixer = require('autoprefixer'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

// style files regexes
const
    cssRegex = /\.css$/,
    cssRegexModule = /\.module\.css$/,
    lessRegex = /\.less$/,
    lessRegexModule = /\.module\.less$/;

const
    isDevelopment = process.env.NODE_ENV === 'development',
    isProduction = !isDevelopment;

const
    mode = isDevelopment ? 'development' : 'production';

const
    nameFiles = (firstName, typeFile) =>
        isDevelopment
            ? `[${firstName}].${typeFile}`
            : `[${firstName}].[contenthash].${typeFile}`; // .[contenthash]

const cssLoaders = (extra) => {
    const loaders = [
        {
            loader: MiniCssExtractPlugin.loader,
            options: {
                sourceMap: isDevelopment
            }
        },
        {
            loader: 'css-loader',
            options: {
                sourceMap: isDevelopment
            }
        },
        {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    autoprefixer({
                        overrideBrowserslist: ['> 0.0001%', 'ie >= 11']
                    }),
                    require('cssnano')({ preset: 'default' }),
                    require('postcss-flexbugs-fixes')
                ],
                sourceMap: isDevelopment
            }
        }
    ];

    if (extra) loaders.push(extra);

    return loaders;
};

const babelOptions = presets => {

    const opts = {
        presets: [
            '@babel/preset-env'
        ],
        sourceMap: isDevelopment
    };

    if (presets) {
        opts.presets.push(presets);
    }

    return opts;
};

module.exports = {
    context: __dirname,
    mode,
    devtool: isDevelopment ? 'source-map' : '',
    entry: {
        common: './src/js/common.js',
        pageSlider: './src/js/home/pageSlider.js',
        home: [
            './src/less/home/index.less',
            './src/js/home/index.js'
        ],
        custom: [
            './src/less/custom/index.less',
            './src/js/custom/index.js'
        ],
        production: [
            './src/less/production/index.less',
            './src/js/production/index.js'
        ],
        job: [
            './src/less/job/index.less',
            './src/js/job/index.js'
        ],
        buy: [
            './src/less/buy/index.less',
            './src/js/buy/index.js'
        ],
    },
    output: isProduction ? {
        path: path.resolve(__dirname, './build/'),
        publicPath: '/build/',
        filename: nameFiles('name', 'js'),
        chunkFilename: nameFiles('name', 'js')
    } : {
        publicPath: '/',
        filename: nameFiles('name', 'js'),
        chunkFilename: nameFiles('name', 'js')
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            '@js': path.resolve(__dirname, './src/js/'),
            '@less': path.resolve(__dirname, './src/less/'),
            '@img': path.resolve(__dirname, `./src/img/`),
            '@src': path.resolve(__dirname, `./src/`),
        }
    },
    devServer: {
        port: 3000,
        publicPath: '/',
        hot: true,
        historyApiFallback: {
            disableDotRule: true
        }
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: 'vendors'
        },
        minimize: true,
        minimizer: [
            new TerserWebpackPlugin({
                cache: false,
                parallel: true,
                sourceMap: isDevelopment,
                extractComments: true,
            }),
        ],
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunks: ['common', 'pageSlider', 'home'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'custom.html' : 'custom',
            template: './src/custom.html',
            chunks: ['common', 'custom'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'production.html' : 'hleb',
            template: './src/production.html',
            chunks: ['common', 'production'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'job.html' : 'job',
            template: './src/job.html',
            chunks: ['common', 'job'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'buy.html' : 'buy',
            template: './src/buy.html',
            chunks: ['common', 'buy'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'pay-success.html' : 'pay-success',
            template: './src/pay-success.html',
            chunks: ['common', 'job'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'error.html' : '404',
            template: './src/404.html',
            chunks: ['common', 'job'],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-default.html' : 'mail-default',
            template: './src/mails/default.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-client-pay.html' : 'mail-pay-client',
            template: './src/mails/client-pay.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-client-cake.html' : 'mail-cake-client',
            template: './src/mails/client-cake.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-admin-cake.html' : 'mail-cake-admin',
            template: './src/mails/admin-cake.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-admin-pay.html' : 'mail-pay-admin',
            template: './src/mails/admin-pay.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-admin-review.html' : 'mail-review-admin',
            template: './src/mails/admin-review.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-admin-feedback.html' : 'mail-admin-feedback',
            template: './src/mails/admin-feedback.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: isProduction ? 'mail-admin-job.html' : 'mail',
            template: './src/mails/admin-job.html',
            chunks: [],
            minify: {
                collapseWhitespace: false
            }
        }),
        new CopyWebpackPlugin( {
            patterns: [
                {
                    from: path.resolve(__dirname, 'src/img'),
                    to: path.resolve(__dirname, 'build/img')
                },
                {
                    from: path.resolve(__dirname, 'src/video'),
                    to: path.resolve(__dirname, 'build/video')
                },
            ]
        }),
        // new BundleAnalyzerPlugin([]), // Показывать ли статистику по пакетам
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: ['!*.woff', '!*.woff2', '!*.ttf', '!*.eot', '!*.otf', '!*.svg', '!*.png', '!*.jpg']
        }),
        new MiniCssExtractPlugin({
            filename: nameFiles('name', 'css'),
            chunkFilename: nameFiles('id', 'css'),
        }),
    ],

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ],
                        sourceMap: isDevelopment
                    }
                }
            },
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: babelOptions('@babel/preset-react')
                }
            },
            {
                test: cssRegex,
                use: cssLoaders(),
            },
            {
                test: lessRegex,
                use: cssLoaders({
                    loader: 'less-loader',
                    options: {
                        sourceMap: isDevelopment
                    }
                }),

            },
            {
                test: lessRegexModule,
                use: cssLoaders({
                    loader: 'less-loader',
                    options: {
                        sourceMap: isDevelopment,
                        importLoaders: 1,
                        modules: true
                    }
                }),
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[contenthash].[ext]',
                    outputPath: 'img',
                },
            },
            {
                test: /\.(mp4)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[contenthash].[ext]',
                    outputPath: 'video',
                },
            },
            {
                test: /\.(ttf|woff|woff2|eot|otf)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[contenthash].[ext]',
                    outputPath: 'fonts',
                },
            },
        ],
    },
};
